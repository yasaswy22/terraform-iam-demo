resource "aws_s3_bucket" "this" {
  count = var.no_buckets
  bucket = "s3-${count.index}-${var.tf_s3_name}"
  acl    = "private"

  tags = {
    "Name" = "s3-${count.index}-${var.root_tag}"
  }
}