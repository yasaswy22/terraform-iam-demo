variable "tf_instance_type" {
  
}

variable "tf_ami_id" {
  
}

variable "tf_ec2_sg" {
  type = list(string)
}

variable "tf_instance_profile" {
  
}

variable "no_instances" {
  
}

variable "root_tag" {
  
}

variable "user_data" {
  
}