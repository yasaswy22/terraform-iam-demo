resource "aws_instance" "this" {
  count = var.no_instances
  instance_type = var.tf_instance_type
  ami = var.tf_ami_id
  vpc_security_group_ids = var.tf_ec2_sg
  iam_instance_profile = var.tf_instance_profile
  user_data = var.user_data

  tags = {
    "Name" = "ec2_${count.index}_${var.root_tag}"
  }
}