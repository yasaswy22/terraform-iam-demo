
resource "aws_iam_role" "this" {
  name = "tf_ec2_s3_access_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "this" {
  name = "tf_ec2_s3_access_policy"
  role = aws_iam_role.this.name
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        "Action" : "s3:*",
        "Resource" : ["*"]
      },
    ]
  })
}

resource "aws_iam_instance_profile" "this" {
  name = "ec2_s3_assoc"
  role = aws_iam_role.this.name
}