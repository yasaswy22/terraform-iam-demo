output "tf_ec2_ssh_sg" {
  value = aws_security_group.ssh_access.id
}

output "tf_ec2_webserver_sg" {
  value = aws_security_group.web_access.id
}