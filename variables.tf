variable "tf_region" {
  type = string
}

variable "tf_access_key" {
  type = string
}

variable "tf_secret_key" {
  type = string
}

variable "tf_ami_id" {
  type = string
  default = "ami-02e136e904f3da870"
}

variable "tf_instance_type" {
  type = string
  default = "t2.micro"
}

variable "tf_ec2_security_group" {
  type = string
  default = "tf_sg"
}

variable "tf_s3_bucket_name" {
  type = string
  default = "tf-practice-bucket"
}

variable "no_of_instances" {
  type = number
  default = 1
}

variable "no_of_buckets" {
  type = number
  default = 1
}

variable "tf_tag_name" {
  default = "tf_practice"
}