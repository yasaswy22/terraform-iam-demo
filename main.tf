provider "aws" {
	access_key = "${var.tf_access_key}"
	secret_key = "${var.tf_secret_key}"
	region = "${var.tf_region}"
}

module "ec2" {
  source = "./modules/ec2"
  no_instances = var.no_of_instances
  tf_instance_type = var.tf_instance_type
  tf_ami_id = var.tf_ami_id
  tf_ec2_sg = ["${module.sgs.tf_ec2_ssh_sg}", "${module.sgs.tf_ec2_webserver_sg}"]
  tf_instance_profile = "${module.policies.tf_instance_profile}"
  user_data = "${file("testcommands.sh")}"
  root_tag = var.tf_tag_name
}

module "sgs" {
  source = "./modules/sgs"
}

module "policies" {
  source = "./modules/policies"
}

module "s3" {
	source = "./modules/s3"
 	no_buckets = var.no_of_buckets
  tf_s3_name = var.tf_s3_bucket_name
  root_tag = var.tf_tag_name
}